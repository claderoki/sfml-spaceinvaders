#include "score.h"



Score::Score(sf::Font &font, unsigned int size) : sf::Text ("Score: 0", font, size)
{
    this->value = 0;
}

void Score::incrementScore()
{
    this->value++;
}

void Score::update()
{
    this->setString("Score: " + std::to_string(this->value));
}