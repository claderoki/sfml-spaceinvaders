#include "lives.h"



Lives::Lives(sf::Font &font, unsigned int size) : sf::Text ("Lives: 3", font, size)
{
    this->value = 3;
}

void Lives::removeLife()
{
    this->value--;
}

int Lives::getLives()
{
    return this->value;
}

void Lives::update()
{
    this->setString("Lives: " + std::to_string(this->value));
}