#pragma once
#include "entity.h"
#include "entityManager.h"
#include "bullet.h"
#include "../score.h"
extern bool gameOver;

class Ship : public Entity
{
public:
    Ship(Score* score, EntityManager* manager, float x, float y);
    void update(sf::RenderWindow* window);
    void collision(Entity* entity);
private:
    EntityManager* manager;
    bool space;
    Score* score;
};