#pragma once
#include "entity.h"
#include "entityManager.h"
#include "../score.h"
#include "bullet.h"

extern float direction;
extern int ammo;


class Enemy : public Entity
{
public:

    Enemy(Lives* lives, EntityManager* manager, float x, float y);
    void update(sf::RenderWindow* window);
    void collision(Entity* entity);
private:
    EntityManager* manager;
    Lives* lives;
};