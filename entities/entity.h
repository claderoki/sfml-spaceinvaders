#pragma once

#include <SFML/Graphics.hpp>
#include <string>
// #include "entityManager.h"

class Entity : public sf::Sprite
{
public:
    int const static SHIP = 1;
    int const static ENEMY = 2;
    int const static SHIPBULLET = 3;
    int const static ENEMYBULLET = 4;

    Entity();

    void load(std::string filename);

    virtual void update(sf::RenderWindow* window);

    virtual void collision(Entity* entity);

    bool checkCollision(Entity* entity);

    int getGroupID();

    int getActive();

    void destroy();

    ~Entity();

protected:
    int groupID;
    int active;
    sf::Vector2f velocity;
private:
    sf::Texture* texture;

};