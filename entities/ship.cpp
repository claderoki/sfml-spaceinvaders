#include "ship.h"

Ship::Ship(Score* score, EntityManager* manager, float x, float y)
{
    this->score = score;
    this->manager = manager;
    this->active = 1;
    this->groupID = Entity::SHIP;
    this->setColor(sf::Color::Green);
    this->load("ball.png");
    this->space = false;
    this->setPosition(x - this->getGlobalBounds().width / 2 ,y - this->getGlobalBounds().height / 2 );
}

void Ship::update(sf::RenderWindow* window)
{
    this->velocity.x = sf::Keyboard::isKeyPressed(sf::Keyboard::D) - sf::Keyboard::isKeyPressed(sf::Keyboard::A);
    if (!this->space && sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
    {
        manager->add("bullet", new Bullet(this->score, this->getPosition().x, this->getPosition().y -32, true) );
    }
    this->space = sf::Keyboard::isKeyPressed(sf::Keyboard::Space);
    Entity::update(window);
}

void Ship::collision(Entity* entity)
{
    switch (entity->getGroupID())
    {
        case 2:
            gameOver = true;
            break;
    }
}