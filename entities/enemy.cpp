#include "enemy.h"

float direction = 0.75f;
int ammo = 5;

Enemy::Enemy(Lives* lives, EntityManager* manager, float x, float y)
{
    this->lives = lives;
    this->manager = manager;
    this->active = 1;
    this->groupID = 2;
    this->setColor(sf::Color::Red);
    this->load("ball.png");
    this->setPosition(x - this->getGlobalBounds().width / 2 ,y - this->getGlobalBounds().height / 2 );
}

void Enemy::update(sf::RenderWindow* window)
{
    if (ammo > 0)
    {
        int chance = rand() % 100 + 1;
        if (chance < 2)
        {
            manager->add("bullet", new Bullet(this->lives, this->getPosition().x, this->getPosition().y + 64 , false));
            ammo -= 1;
        }
    }

    if (this->velocity.x != direction)
    {
        this->move(0,32);
    }
    this->velocity.x = direction;

    Entity::update(window);
    if (this->getPosition().x <= 0 || this->getPosition().x + this->getGlobalBounds().width >= window->getSize().x )
    {
        direction *= -1;
        this->velocity.x = direction;
        this->move(0,32);
        Entity::update(window);
        Entity::update(window);
    }
}

void Enemy::collision(Entity* entity)
{
    // switch(entity->getGroupID())
    // {
    //     case 0:
    //         break;
    //     case Entity::SHIPBULLET:
    //         entity->destroy();
    //         this->destroy();
    //         break;
    // }
}