#pragma once

#include <unordered_map>
#include <string>
#include <vector>
#include "entity.h"

class EntityManager
{
public:
    EntityManager();
    void add(std::string name, Entity* entity);
    void update(sf::RenderWindow* window);
    Entity* get(std::string name);
    void render(sf::RenderWindow* window);
    ~EntityManager();
private:
    std::unordered_map<std::string, Entity* > entities;
};