#include "entity.h"

Entity::Entity()
{
    this->texture = new sf::Texture();
    this->active = 1;
    this->groupID = 0;
}

void Entity::load(std::string filename)
{
    this->texture->loadFromFile("resources/sprites/" + filename);
    this->setTexture(*this->texture);
}

bool Entity::checkCollision(Entity* entity)
{
    return this->getGlobalBounds().intersects(entity->getGlobalBounds());
}


void Entity::update(sf::RenderWindow* window)
{
    this->move(this->velocity);
}

void Entity::collision(Entity* entity)
{

}


int Entity::getGroupID()
{
    return this->groupID;
}

int Entity::getActive()
{
    return this->active;
}
void Entity::destroy()
{
    this->active = 0;
}
Entity::~Entity()
{
    delete this->texture;
    this->active = 0;
}
