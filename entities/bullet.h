#pragma once
#include "entity.h"
#include "../score.h"
#include "../lives.h"

extern int ammo;
extern bool gameOver;
extern int enemyCount;

class Bullet : public Entity
{
public:
    Bullet(Score* score, float x, float y, bool good);
    Bullet(Lives* lives, float x, float y, bool good);
    void update(sf::RenderWindow* window);
    void collision(Entity* entity);
private:
    Score* score;
    Lives* lives;
};