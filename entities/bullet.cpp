#include "bullet.h"

Bullet::Bullet(Score* score, float x, float y, bool good)
{
    this->score = score;
    if (good)
    {
        this->velocity.y = -1;
        this->groupID = Entity::SHIPBULLET;
    }
    else
    {
        this->velocity.y = 1;
        this->groupID = Entity::ENEMYBULLET;
    }

    this->active = 1;
    this->setColor(sf::Color::White);
    this->load("ball.png");
    this->setScale(0.25f,0.5f);
    this->setPosition(x - this->getGlobalBounds().width / 2 ,y - this->getGlobalBounds().height / 2 );
}


Bullet::Bullet(Lives* lives, float x, float y, bool good)
{
    this->lives = lives;
    if (good)
    {
        this->velocity.y = -1;
        this->groupID = Entity::SHIPBULLET;
    }
    else
    {
        this->velocity.y = 1;
        this->groupID = Entity::ENEMYBULLET;
    }

    this->active = 1;
    this->setColor(sf::Color::White);
    this->load("ball.png");
    this->setScale(0.25f,0.5f);
    this->setPosition(x - this->getGlobalBounds().width / 2 ,y - this->getGlobalBounds().height / 2 );
}

void Bullet::update(sf::RenderWindow* window)
{
    if (this->getPosition().y <= 0 || this->getPosition().y + this->getGlobalBounds().height >= window->getSize().y )
    {
        if (this->groupID == Entity::ENEMYBULLET)
        {
            ammo++;
        }
        this->destroy();
    }
    Entity::update(window);
}



void Bullet::collision(Entity* entity)
{
    if (this->groupID == Entity::SHIPBULLET)
    {
        switch(entity->getGroupID())
        {
            case Entity::SHIP:
            case Entity::SHIPBULLET:
            case Entity::ENEMYBULLET:
                break;
            case Entity::ENEMY:
                enemyCount--;
                entity->destroy();
                this->score->incrementScore();
                this->destroy();
                break;
        }
    }
    else if (this->groupID == Entity::ENEMYBULLET)
    {
        switch(entity->getGroupID())
        {
            case Entity::ENEMY:
            case Entity::SHIPBULLET:
            case Entity::ENEMYBULLET:
                break;
            default:
                this->lives->removeLife();
                ammo += 1;
                this->destroy();
                if (this->lives->getLives() < 1)
                {
                    gameOver = true;
                }
                break;
        }
    }


}