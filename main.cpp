#include <SFML/Graphics.hpp>

#include "states/gameState.h"
#include "states/mainMenu.h"

#include <chrono>
#include <thread>

GameState coreState;
bool quitGame = false;

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "Ping");

    coreState.setWindow(&window);
    coreState.setState(new MainMenu());

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        window.clear(sf::Color::Black);

        coreState.update();
        coreState.render();

        window.display();

        if (quitGame)
        {
            window.close();
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(2));
    }

    return 0;
}