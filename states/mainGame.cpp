#include "mainMenu.h"

bool gameOver = false;
int enemyCount = 0;

void MainGame::initialize(sf::RenderWindow* window)
{
    this->font = new sf::Font();
    this->font->loadFromFile("resources/fonts/font.ttf");

    this->score = new Score(*font, 64U);
    this->lives = new Lives(*font, 64U);
    this->lives->setPosition(window->getSize().x - this->lives->getGlobalBounds().width, 0);
    this->manager = new EntityManager();

    this->manager->add("ship", new Ship(this->score,this->manager, window->getSize().x ,window->getSize().y ));

    
    for (int y = 0; y < 5; y++)
    {
        for (int x = 0; x < 10; x++)
        {
            this->manager->add("enemy", new Enemy(this->lives, this->manager, 16 + 48 * x, 32 + 48 * y));
            enemyCount++;
        }
    }
}

void MainGame::update(sf::RenderWindow* window)
{

    this->score->update();
    this->lives->update();
    this->manager->update(window);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
    {
        coreState.setState(new MainMenu());
        return;
    }
    if (enemyCount == 0)
    {
        coreState.setState( new WinScreen());
        return;
    }
    if (gameOver)
    {
        gameOver = false;
        coreState.setState(new GameOver());
        return;
    }
}

void MainGame::render(sf::RenderWindow* window)
{
    this->manager->render(window);
    window->draw(*this->score);
    window->draw(*this->lives);
}


void MainGame::destroy(sf::RenderWindow* window)
{
    delete this->font;
    delete this->score;
    delete this->lives;
    delete this->manager;
}
