#include "gameOver.h"


void GameOver::initialize(sf::RenderWindow* window)
{
    this->font = new sf::Font();
    this->font->loadFromFile("resources/fonts/font.ttf");

    this->gameOverText = new sf::Text("Game over! Press enter or die.", *this->font, 64U);
    this->gameOverText->setOrigin(this->gameOverText->getGlobalBounds().width / 2, this->gameOverText->getGlobalBounds().height / 2);
    this->gameOverText->setPosition(window->getSize().x / 2, window->getSize().y / 10);
}

void GameOver::update(sf::RenderWindow* window)
{
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && this->enterKey)
    {
        coreState.setState( new MainMenu() );
        return;
    }
    this->enterKey = sf::Keyboard::isKeyPressed(sf::Keyboard::Return);
}

void GameOver::render(sf::RenderWindow* window)
{
    window->draw(*this->gameOverText);
}

void GameOver::destroy(sf::RenderWindow* window)
{
    delete this->font;
    delete this->gameOverText;
}
