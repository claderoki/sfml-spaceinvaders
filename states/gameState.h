#pragma once
#include <SFML/Graphics.hpp>



class TinyState
{
public:
    virtual void initialize(sf::RenderWindow* window)
    {
    }

    virtual void update(sf::RenderWindow* window)
    {
    }

    virtual void render(sf::RenderWindow* window)
    {
    }

    virtual void destroy(sf::RenderWindow* window)
    {
    }
};

class GameState
{
public:
    GameState()
    {
        this->state = NULL;
    }

    void setWindow(sf::RenderWindow* window)
    {
        this->window = window;
    }

    void setState(TinyState* state)
    {
        if (this->state != NULL)
        {
            this->state->destroy(this->window);
        }
        this->state = state;
        if (this->state != NULL)
        {
            this->state->initialize(this->window);
        }
}

    void update()
    {
        if (this->state != NULL)
        {
            this->state->update(this->window);
        }
    }
    void render()
    {
        if (this->state != NULL)
        {
            this->state->render(this->window);
        }
    }

    ~GameState()
    {
        if (this->state != NULL)
        {
            this->state->destroy(this->window);
        }
    }
private:
    sf::RenderWindow* window;
    TinyState* state;
};

extern GameState coreState;
extern bool quitGame;