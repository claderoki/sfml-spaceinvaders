#pragma once
#include "gameState.h"
#include <SFML/Graphics.hpp>
#include "mainGame.h"

class MainMenu: public TinyState
{
public:
    void initialize(sf::RenderWindow* window);
    void update(sf::RenderWindow* window);
    void render(sf::RenderWindow* window);
    void destroy(sf::RenderWindow* window);
private:
    int selected;
    bool upKey, downKey;

    sf::Font* font;
    sf::Text* title;
    sf::Text* play;
    sf::Text* quit;
};