#pragma once
#include "gameState.h"
#include <SFML/Graphics.hpp>
#include "mainMenu.h"

class GameOver: public TinyState
{
public:
    void initialize(sf::RenderWindow* window);
    void update(sf::RenderWindow* window);
    void render(sf::RenderWindow* window);
    void destroy(sf::RenderWindow* window);
private:
    bool enterKey;
    sf::Font* font;
    sf::Text* gameOverText;
};