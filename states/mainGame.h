#pragma once
#include "gameState.h"
#include "gameOver.h"
#include "winScreen.h"


#include "../entities/entityManager.h"
#include "../entities/enemy.h"
#include "../entities/ship.h"
#include "../score.h"
#include "../lives.h"

extern bool gameOver;
extern GameState coreState;
extern int enemyCount;

#include <SFML/Graphics.hpp>


class MainGame: public TinyState
{
public:
    void initialize(sf::RenderWindow* window);
    void update(sf::RenderWindow* window);
    void render(sf::RenderWindow* window);
    void destroy(sf::RenderWindow* window);
private:
    sf::Font* font;
    Score* score;
    Lives* lives;
    EntityManager* manager;
};