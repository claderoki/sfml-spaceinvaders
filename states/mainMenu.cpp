#include "mainMenu.h"


void MainMenu::initialize(sf::RenderWindow* window)
{
    this->selected = 0;

    this->font = new sf::Font();
    this->font->loadFromFile("resources/fonts/font.ttf");

    this->title = new sf::Text("Space Invaders", *this->font, 64U);
    this->title->setOrigin(this->title->getGlobalBounds().width / 2, this->title->getGlobalBounds().height / 2);
    this->title->setPosition(window->getSize().x / 2, window->getSize().y / 10);

    this->play = new sf::Text("play", *this->font, 128U);
    this->play->setOrigin(this->play->getGlobalBounds().width / 2, this->play->getGlobalBounds().height / 2);
    this->play->setPosition(window->getSize().x / 2, window->getSize().y / 2.50);

    this->quit = new sf::Text("quit", *this->font, 128U);
    this->quit->setOrigin(this->quit->getGlobalBounds().width / 2, this->quit->getGlobalBounds().height / 2);
    this->quit->setPosition(window->getSize().x / 2, window->getSize().y / 1.50);

}

void MainMenu::update(sf::RenderWindow* window)
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Up) && !this->upKey)
    {
        this->selected -= 1;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Down) && !this->downKey)
    {
        this->selected += 1;
    }

    if (this->selected > 1)
    {
        this->selected = 0;
    }

    if (this->selected < 0)
    {
        this->selected = 1;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
    {
        switch (this->selected)
        {
            case 0:
                coreState.setState( new MainGame() );
                break;
            case 1:
                quitGame = true;
                break;
        }
    }
    this->upKey = sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
    this->downKey = sf::Keyboard::isKeyPressed(sf::Keyboard::Down);

}

void MainMenu::render(sf::RenderWindow* window)
{
    this->play->setFillColor(sf::Color::White);
    this->quit->setFillColor(sf::Color::White);
    switch (this->selected)
    {
    case 0:
        this->play->setFillColor(sf::Color::Blue);
        break;
    case 1:
        this->quit->setFillColor(sf::Color::Blue);
        break;
    }
    window->draw(*this->title);
    window->draw(*this->play);
    window->draw(*this->quit);


}

void MainMenu::destroy(sf::RenderWindow* window)
{
    delete this->font;
    delete this->title;
    delete this->play;
    delete this->quit;
}
