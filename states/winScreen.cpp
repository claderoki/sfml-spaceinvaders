#include "gameOver.h"


void WinScreen::initialize(sf::RenderWindow* window)
{
    this->font = new sf::Font();
    this->font->loadFromFile("resources/fonts/font.ttf");

    this->winScreenText = new sf::Text("You won!", *this->font, 64U);
    this->winScreenText->setOrigin(this->winScreenText->getGlobalBounds().width / 2, this->winScreenText->getGlobalBounds().height / 2);
    this->winScreenText->setPosition(window->getSize().x / 2, window->getSize().y / 10);
}

void WinScreen::update(sf::RenderWindow* window)
{
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && this->enterKey)
    {
        coreState.setState( new MainMenu() );
        return;
    }
    this->enterKey = sf::Keyboard::isKeyPressed(sf::Keyboard::Return);
}

void WinScreen::render(sf::RenderWindow* window)
{
    window->draw(*this->winScreenText);
}

void WinScreen::destroy(sf::RenderWindow* window)
{
    delete this->font;
    delete this->winScreenText;
}
