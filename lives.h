#pragma once 
#include <SFML/Graphics.hpp>

class Lives : public sf::Text
{
public:
    Lives(sf::Font &font, unsigned int size);
    void removeLife();
    void update();
    int getLives();
private:
    int value;
};